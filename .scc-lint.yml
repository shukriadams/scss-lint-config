# One Config To Rule Them All
# Author : Shukri Adams (shukri.adams@gmail.com)
# Linter documentation https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md


# This will lint everything in all subfolders relative to current path. This config is intended for use
# from Grunt or some other runner which passes a more specific path in.
scss_files: '**/*.scss'


# Default linter folder, relative to execution folder
plugin_directories: ['.scss-linters']


linters:

    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#bangformat
    # Reason : Readability
    # Best practice : Include a space before "!" when using commands like "!default"
    #                 $width: 10px !default;
    #
    BangFormat:
        enabled: true


    # https://github.com/shukriadams/cardinalPercentageWidths
    # Reason : Arbitrary percentages width often scale strangely in responsive design - percent 
    #          widths are best used to set up column-like structures. 
    # Best practice : When using % width, use 100% (full-width) 50% (half-width), 33% and 25%.
    #
    CardinalPercentageWidths:
        enabled: true
        values: [0, 33, 50, 66, 25, 100]


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#chainedclasses
    # Reason : Chained classes are unnecessary and create tight coupling. They also increase selector 
    #          specificity, forcing overrides to duplicate the chaining.
    # Best practice : Create a unique class for the element you want to select.
    #
    ChainedClasses:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#colorkeyword
    # Reason : colors should be defined as hex values, not names like "black" or "red", as these 
    #          primitive color values have no place in good design.
    # Best practice : Use hex values instead of names. 
    #
    ColorKeyword:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#colorvariable
    # Reason : Colors defined as static values in SCSS are difficult to manage and change. Good design 
    #          has a limited number of colors.
    # Best practice : Colors should be defined as SASS variables, and all variables should be defined 
    #                 in a single color palette file. 
    #
    ColorVariable:
        enabled: true
  
    
    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#comment
    # This linter is disabled. Comment to your heart's content.
    #
    Comment:
        enabled: false


    # https://github.com/shukriadams/componentFileCheck
    # Reason :  CSS classes which are not tied to a BEM module are difficult to manage and reuse, and BEM
    #           modules which are not tied to files are difficult to manage and reuse.
    # Best practice : Put CSS for a module in a file with the same name as the module. Always namespace 
    #                 your classes to a module.
    #
    ComponentFileCheck:
        enabled: true
        glob: 'src/**/*.scss'


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#declarationorder
    # Reason : readibility.
    # Best practice : put SASS mixins/functions first, CSS properties next and child style last.
    #                 .fatal-error {
    #                     @include someMixin();
    #                     color: $myColor;
    #
    #                     p {
    #                         ...
    #                     }
    #                  }
    #  
    DeclarationOrder:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#disablelinterreason
    # Reason : When disabling a linter, it is good practice to explain why.
    # Best practice : Add a short explanatio comment above the disabling command.
    #
    DisableLinterReason:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#duplicateproperty
    # Reason : duplicate css properties are unnecessary.
    # Best practice : Don't duplicate properties in a class.
    #
    DuplicateProperty:
        enabled: true
        ignore_consecutive:
            - background


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#emptylinebetweenblocks
    # Reason : readability
    # Best solution : Add a space after each class declaration.
    #
    EmptyLineBetweenBlocks:
        enabled: true
        ignore_single_line_blocks : false


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#emptyrule
    # Reason : Css classes are empty.
    # Best practice : don't create empty classes like 
    #                 .cat {
    #                 }
    #
    EmptyRule:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#finalnewline
    # Reason : files should always end in a newline.
    # Best practice : end files with a new line.
    #
    FinalNewline:
        enabled: true


    # https://github.com/shukriadams/flaggedPropertiesAndValues
    # Reason : Some properties like float are best avoided.
    # Best practice : Avoid properties which can be replaced with more robust methods
    #
    FlaggedPropertiesAndValues:
        enabled: true
        properties: [float]
        values: [initial]


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#hexlength
    # This linter is disabled. Comment to your heart's content.
    #
    HexLength:
        enabled: false


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#hexvalidation
    # Reason : an invalid hex was detected.
    # Best practice : use a valid hex.
    #
    HexValidation:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#idselector
    # Reason : ID selectors are discouraged because IDs are not intended to be style selectors.
    # Best practice : Don't use ID selectors.
    #
    IdSelector:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#importantrule
    # Reason : !important is a terrible way to solve problems in CSS, as it breaks natural cascade inheritance.
    # Best practice : don't use !important - if you need it, you're probably doing something wrong.
    # 
    ImportantRule:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#importpath
    # Reason : you added an import path either a _ prefix or a .scss extention - both are unnecessary.
    # Best practice : dont' use _ or .scss.
    #
    ImportPath:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#indentation
    # Reason : You didn't indent 4 spaces.
    # Best practice : Indent with 4 spaces.
    # 
    Indentation:
        enabled: true
        width: 4
        
        
    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#leadingzero
    # Reason : Leading zero's are unnecessary.
    # Best practice : don't use leading zero's.
    #
    LeadingZero:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#mergeableselector
    # Reason : You have declared a selector more than once, and the declarations can be merged.
    # Best practice : Merge the selectors.
    #
    MergeableSelector:    
        enabled: true


    # https://github.com/shukriadams/modifierRelatedToParent
    # Reason : You declared a modifier which does not share a base name with 
    # Best practice : Use  "--" and modifier name suffix.
    # A modifier class should be named for the class it modifies so you don't use modifiers on the wrong classes, or sit with a "loose" modifier and don't what it belongs to.  
    #
    ModifierRelatedToParent:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#nameformat
    # This linter is disabled.
    #
    NameFormat:
        enabled: false


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#nestingdepth
    # Reason : Selectors should not be too deep, as they are overly-specific and difficult to modify.
    # Best practice : Limit your selector to two levels.
    #
    NestingDepth:
        enabled: true
        max_depth: 2


    # https://github.com/shukriadams/noClassElementSelectors
    # Reason : Class.element chained selectors are unnecessary
    # Best practice : Don't use class.element chained selectors - give your element a unique class name instead.
    # Exceptions : <li> are usually permitted.
    #
    NoClassElementSelectors:
        enabled: true
        ignoredElements: [li, option]

    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#placeholderinextend
    # This linter is disabled.    
    #
    PlaceholderInExtend:
        enabled: false


    # https://github.com/shukriadams/preventExtend
    # Reason : Class extend in SCSS can produce tight coupling and compile sequence dependency between two modules, 
    #          and CSS already has extension built in at render-time.
    # Best practice : Avoid using SCSS extend.
    #
    PreventExtend:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#propertysortorder
    # Reason : Readability.
    # Best practice : Order CSS properties alphabetically.
    # 
    PropertySortOrder:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#propertyspelling
    # Reason : You made a typo in a property name.
    # Best practice : Use proper property name spelling. 
    #
    PropertySpelling:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#qualifyingelement
    # Reason: Adding elements to class selectors are unnecessarily specific and force coupling classes to elements.
    # Best practice : Avoid qualifying elements in your selectors, egs, don't do
    #                 ul.list {
    #                     ...    
    #                 }
    #                 
    #                 when the following is enough :
    # 
    #                 .list {
    #                     ...    
    #                 }
    #
    QualifyingElement:
        enabled: true


    # https://github.com/shukriadams/scopedComponentOverrides
    # Reason : when a complex module consumes and overrides a simple module, those overrides should always be limited to that complex module.
    #          In this way multiple complex modules can override the same simple module without affecting eachother.
    #
    #
    ScopedComponentOverrides:
        enabled: true
        ignoredComponents : []


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#selectordepth
    # Reason : Selector depth creates overly-specific selectors.
    # Best practice : Limit your selector depth to 2
    #                 .one .two {
    #                      ...
    #                 } 
    #
    SelectorDepth:
        enabled: true
        max_depth: 2


    # https://github.com/shukriadams/selectorConcatenation
    # Reason : SASS' concatentate selectors are difficult to refactor and read.
    # Best practice : Don't use selector concatenation like
    #                 .my { 
    #                     &Selector {
    #                          ...
    #                     }    
    #                 }
    #
    #                 Instead use
    #
    #                 .mySelector {
    #                     ...
    #                 }
    #
    SelectorConcatenation:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#selectorformat
    # Reason : class name needs to conform to a fixed format so its context and purpose are clear.
    # Best practice : Use the shortened BEM format of domain_block-element--modifier. Camelcase is 
    #                 permitted - use it!
    #
    SelectorFormat:
        enabled: true
        #lowercase & any letters _ lowecase & any letters (optional - lowecase & any letters)
        convention: ^([a-z0-9](([a-zA-Z0-9]+)?)_)?[a-z0-9](([a-zA-Z0-9]+)?)((-[a-z0-9]([a-zA-Z0-9]+)?)?)((--[a-z0-9]([a-zA-Z0-9]+)?)?)$
        ignored_types : [element]
        convention_explanation: Use proper classname format 'module-element--modifer or domain_block-element--modifer'


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#shorthand
    # This linter is disabled.  
    #
    Shorthand:
        enabled: false


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#singlelineperproperty
    # Reason : readability
    # Best practice : limit your properties to one per line
    #                 p {
    #                     margin: 0; padding: 0;   // bad
    #                 }   
    #                
    #                 p {
    #                     margin: 0;               // good
    #                     padding: 0;
    #                 }
    # 
    SingleLinePerProperty:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#singlelineperselector
    # Reason : readability
    # Best practice : Limit your selectors to one per line
    #                 .error, .explanation {  
    #                     ... bad
    #                 }
    #
    #                 .error, 
    #                 .explanation {  
    #                     ... good
    #                 }
    #
    SingleLinePerSelector:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#spaceaftercomma
    # Reason : Readability
    # Best practice : Add a space after comma
    #                 color: rgba(0,0,0,.2)        // bad
    #                 color: rgba(0, 0, 0, .2)     // good
    #
    SpaceAfterComma:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#spaceafterpropertycolon
    # Reason : readability
    # Best practice : Use one space after a property colon.
    #                 margin:0;    // bad
    #                 maring: 0;   // good
    #
    SpaceAfterPropertyColon:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#spaceafterpropertyname
    # Reason : readability
    # Best practice : Dont use spaces after property names.
    #                 margin : 0;      // bad 
    #                 margin: 0;       // good
    SpaceAfterPropertyName:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#spaceaftervariablecolon
    # Reason : readability
    # Best practice : Use one space after the variable assigment colon and none before.
    #                 $my-var : 0;    // bad 
    #                 $my-var: 0;     // good
    SpaceAfterVariableName:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#spacebeforebrace
    # Reason : readability
    # Best practice : Add a space between class name and brace.
    #                 p{ }       // bad
    #                 p { }      // good
    #
    SpaceBeforeBrace:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#stringquotes
    # Reason : consistency
    # Best practice : content property should use single quotes, not double
    #
    StringQuotes:
        enabled: true
        
        
    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#trailingsemicolon
    # This linter is disabled.  
    #
    TrailingSemicolon:
        enabled: false


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#trailingwhitespace
    # This linter is disabled.  
    TrailingWhitespace:
        enabled: false    


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#unnecessaryparentreference
    # Reason : unnecessary
    # Best practice : Do not use parent selector references (&) when they would otherwise be unnecessary.
    UnnecessaryParentReference:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#variableforproperty
    # Reason : color and font are easier to maintain if declared as variables.
    # Best practice : Use variables for fonts and colors.
    #
    VariableForProperty:
        enabled: true


    # https://github.com/shukriadams/variableNamedToComponent
    # Reaspon : variables should be named after the components they are declared in to give them better context.
    # Best practice : prefix your variables with the name of the component it is declared in, excample
    #                 $myComponent-someVar: 10;
    #
    VariableNamedToComponent:
        enabled: true
        divider: "-"
        whitelistedPartials : ['gloo', 'base-colorPalette']


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#vendorprefix
    # Reason : Avoid writing your own vendor prefixes, this bloats source codes and list is not always complete.
    # Best practice : Use the provided vendor mixin to wrap properties which require a vendor prefix.
    #
    VendorPrefix:
        enabled: true

    # https://github.com/shukriadams/vendorPrefixRequired
    # Reason : Some properties require vendor prefixes to work cross-browser.
    # Best practice : Ensure that properties which require vendor prefixes are wrapped with the provided vendor
    #                 mixin.
    #
    VendorPrefixRequired:
        enabled: true


    # https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md#zerounit
    # Reason : Units are not require for zero values.
    # Best practice : Avoid using units on zero values.
    # 
    ZeroUnit:
        enabled: true

